FROM php:7.2

RUN apt-get update && apt-get install -y git zlib1g-dev supervisor libzip-dev
RUN docker-php-ext-install zip

# COPY docker/supervisord/wecamp_bot.conf /etc/supervisord/conf.d/wecamp_bot.conf

ADD . /var/bot

WORKDIR /var/bot

RUN cd /var/bot && ./composer.phar install
# RUN service supervisor start

# This does not seem to work yet :(
# CMD ["/usr/bin/supervisord -c /var/bot/docker/supervisord/wecamp_bot.conf"]

CMD php /var/bot/bot.php
